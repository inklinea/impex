#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Impex - Import and Export Fun
# An Inkscape 1.1+ extension
##############################################################################
import io

import inkex
from inkex import command, Group

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf

from PIL import Image, ImageOps

import os
from datetime import datetime
from pathlib import Path
import configparser
import tempfile
import shutil
import base64
from io import BytesIO
from lxml import etree
import random

conversions = {
    'in': 96.0,
    'pt': 1.3333333333333333,
    'px': 1.0,
    'mm': 3.779527559055118,
    'cm': 37.79527559055118,
    'm': 3779.527559055118,
    'km': 3779527.559055118,
    'Q': 0.94488188976378,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0,
    '': 1.0,  # Default px
}


def create_new_group(self, prefix, mode):
    group_id = str(prefix) + '_' + str(random.randrange(100000, 999999))
    new_group = self.svg.add(Group.new('#' + group_id))
    new_group.set('inkscape:groupmode', str(mode))
    new_group.attrib['id'] = group_id
    return new_group


def pil_base64(image, img_format, jpeg_quality):
    img_stream = BytesIO()
    if img_format == 'png':
        image.save(img_stream, format='PNG')
    else:
        image = image.convert('RGB')
        image.save(img_stream, format='JPEG', quality=jpeg_quality)
    byte_img = img_stream.getvalue()
    base64_img_str = base64.b64encode(byte_img)
    return base64_img_str


def base64_image_to_canvas(parent, base64_string, image_width, image_height, cf, x_offset, image_number, base_id, format):
    my_image = etree.SubElement(parent, inkex.addNS('image', 'svg'))
    my_image.attrib['x'] = str(0 + x_offset)
    my_image.attrib['y'] = str(0)

    my_image.attrib['width'] = str(image_width / cf)
    my_image.attrib['height'] = str(image_height / cf)
    my_image.attrib['id'] = f'{base_id}_{image_number}'
    # my_image.attrib['display'] = 'visible'
    if format == 'png':
        my_image.set('xlink:href', str(f'data:image/png;base64,{base64_string.decode()}'))
    else:
        my_image.set('xlink:href', str(f'data:image/jpeg;base64,{base64_string.decode()}'))
    return my_image


def base64_image_to_frame(self, base64_string, frame_object, cf):
    parent = self.svg
    my_image = etree.SubElement(parent, inkex.addNS('image', 'svg'))
    my_image.attrib['x'] = str(frame_object.bounding_box().left / cf)
    my_image.attrib['y'] = str(frame_object.bounding_box().top / cf)

    my_image.attrib['width'] = str(frame_object.bounding_box().width / cf)
    my_image.attrib['height'] = str(frame_object.bounding_box().height / cf)
    my_image.attrib['id'] = 'test_image'

    my_image.set('xlink:href', str(f'data:image/png;base64,{base64_string.decode()}'))

    return my_image


def import_text_file(self, import_file_path):
    with open(import_file_path) as text_file:
        contents = text_file.read()
        # inkex.errormsg(contents)
    parent = self.svg
    my_text = etree.SubElement(parent, inkex.addNS('text', 'svg'))
    my_text.attrib['x'] = str(0)
    my_text.attrib['y'] = str(0)
    my_text.text = contents

    return my_text


def import_gif_chooser(self, my_image):
    if my_image.n_frames > 1:
        Handler.animated_gif_import_window_show(self, my_image)
        preview_animated_gif(self, my_image)
    else:
        import_gif(self, my_image)


def import_gif(self, my_image):
    my_image_width = my_image.size[0]
    my_image_height = my_image.size[1]
    x_offset = 0

    for frame in range(0, my_image.n_frames):
        image_number = frame
        my_image.seek(frame)
        base64_string = pil_base64(my_image, 'png', 85)
        base64_image_to_canvas(self, base64_string, my_image_width, my_image_height, cf, x_offset, image_number,
                               'imported_image', 'png')
        x_offset += my_image_width / cf
    my_image.close()


# def preview_animated_gif(self, my_image):
#     gif_stack = builder.get_object('animated_gif_stack')
#
#     # Remove existing stack images
#     gif_stack_children = gif_stack.get_children()
#     for child in gif_stack_children:
#         gif_stack.remove(child)
#
#     for frame in range(0, my_image.n_frames):
#         my_image.seek(frame)
#
#         # Frame durations are stored in info
#         frame_duration = my_image.info['duration']
#         # inkex.errormsg(f'Frame No: {frame} Duration {frame_duration}')
#
#         my_frame = my_image
#         my_frame.thumbnail((224, 224), Image.ANTIALIAS)
#
#         my_frame_filename = f'frame_{frame}.gif'
#         temp_gif_path = os.path.join(my_temp_folder, my_frame_filename)
#
#         my_frame.save(temp_gif_path)
#         gtk_image = Gtk.Image.new_from_file(temp_gif_path)
#
#         gif_stack.add_named(gtk_image, f'test_image_{frame}')
#         # inkex.errormsg(f'frame{frame}')
#         gtk_image.show()
#
#     my_adjustment = builder.get_object('animated_gif_frame_adjustment')
#     my_adjustment.set_upper(my_image.n_frames - 1)
#     my_adjustment.set_value(int(my_image.n_frames / 2))
#
#     my_image.close()

def preview_animated_gif(self, my_image):
    gif_stack = builder.get_object('animated_gif_stack')

    # Remove existing stack images
    gif_stack_children = gif_stack.get_children()
    for child in gif_stack_children:
        gif_stack.remove(child)

    for frame in range(0, my_image.n_frames):
        my_image.seek(frame)

        current_frame = my_image.convert('RGBA')
        current_frame.save(os.path.join(my_temp_folder, f'temp_frame_{frame}.png'))

        if frame > 0:

            prev_image = Image.open(os.path.join(my_temp_folder, f'temp_frame_{frame - 1}.png'))
            my_new_image = Image.alpha_composite(current_frame, prev_image)
            # current_frame.paste(prev_image)
            # my_new_image = current_frame
            my_frame = my_new_image

        else:
            my_frame = current_frame

        my_frame_filename = f'frame_{frame}.png'
        temp_gif_path = os.path.join(my_temp_folder, my_frame_filename)
        my_frame.save(temp_gif_path)

        my_frame.thumbnail((224, 224), Image.ANTIALIAS)

        my_frame_filename_thumbnail = f'frame_thumbnail_{frame}.png'
        temp_gif_path_thumbnail = os.path.join(my_temp_folder, my_frame_filename_thumbnail)

        my_frame.save(temp_gif_path_thumbnail)
        gtk_image = Gtk.Image.new_from_file(temp_gif_path_thumbnail)

        gif_stack.add_named(gtk_image, f'frame_thumbnail_{frame}')

        gtk_image.show()

    my_adjustment = builder.get_object('animated_gif_frame_adjustment')
    my_adjustment.set_upper(my_image.n_frames - 1)
    # my_adjustment.set_value(int(my_image.n_frames / 2))
    my_adjustment.set_value(0)

    my_image.close()

def single_gif_frame_to_canvas(self):

    base_id = 'gif_frame'
    x_offset = 0
    frame_number = str(int(builder.get_object('animated_gif_scale').get_value()))
    my_frame_path = current_frame_path = os.path.join(my_temp_folder, f'frame_{frame_number}.png')
    my_frame = Image.open(my_frame_path)
    my_frame_width = my_frame.size[0]
    my_frame_height = my_frame.size[1]

    base64_string = pil_base64(my_frame, 'png', 85)
    base64_image_to_canvas(inkex_self.svg, base64_string, my_frame_width, my_frame_height, cf, x_offset, frame_number, base_id, 'png')

# Function to convert an animated gif into an keyframed svg
# Can be base64 png or jpeg frames
def anim_gif_to_anim_svg(self, is_animated):
    import_file_path = builder.get_object('importFileChooserWidget').get_filename()

    # Create a layer for the frames
    my_layer = create_new_group(inkex_self, 'gif_layer', 'layer')

    base_id = 'gif_frame'
    x_offset = 0

    my_image = Image.open(import_file_path)

    my_frame_width = my_image.size[0]
    my_frame_height = my_image.size[1]

    key_times = [0.0]
    animation_value = ['none']
    cumulative_frame_time = float(0)

    for frame in range(0, my_image.n_frames):

        my_image.seek(frame)

        # Load the frames which were saved during preview generation
        current_frame_path = os.path.join(my_temp_folder, f'temp_frame_{frame}.png')
        current_frame = Image.open(current_frame_path)
        base64_string = pil_base64(current_frame, 'png', 85)
        base64_image_to_canvas(my_layer, base64_string, my_frame_width, my_frame_height, cf, x_offset, frame, base_id, 'png')

        frame_duration = my_image.info['duration']
        frame_key_time = float(frame_duration)
        cumulative_frame_time += frame_key_time
        key_times.append(cumulative_frame_time)
        animation_value.append('none')

    my_image.close()

    if is_animated == False:
        return

    # Convert all keytimes to fraction of total duration
    # Should give float from 0 to 1

    total_duration = key_times[-1]
    key_times = [round(x / key_times[-1], 3) for x in key_times]

    key_times = [str(x) for x in key_times]

    key_times_str = ';'.join(key_times)
    # animation_value = ';'.join(animation_value)
    # inkex.errormsg(key_times)
    # inkex.errormsg(animation_value)

    my_animation = etree.SubElement(my_layer, inkex.addNS('animate', 'svg'))
    my_animation.attrib['attributeName'] = 'display'
    my_animation.attrib['begin'] = '0.5s'
    my_animation.attrib['dur'] = f'{str(total_duration / 1000)}s'
    my_animation.attrib['keyTimes'] = key_times_str
    # my_animation.attrib['values'] = animation_value
    my_animation.attrib['repeatCount'] = 'indefinite'
    my_animation.attrib['fill'] = 'freeze'
    my_animation.attrib['accumulate'] = 'sum'
    my_animation.attrib['calcMode'] = 'discrete'
    my_animation.attrib['additive'] = 'sum'
    # add animation element to each image element

    for frame in (range(0, my_image.n_frames)):
        my_image_element = inkex_self.svg.getElementById(f'gif_frame_{frame}')
        my_animation_element = my_animation.duplicate()
        my_image_element.append(my_animation_element)
        my_animation_values = animation_value.copy()
        my_animation_values[frame] = 'block'

        my_animation_element.attrib['values'] = ';'.join(my_animation_values)

    my_animation.delete()


def basic_import_file(self, my_image):
    my_image_width = my_image.size[0]
    my_image_height = my_image.size[1]
    base64_string = pil_base64(my_image, 'png', 85)
    base64_image_to_canvas(self, base64_string, my_image_width, my_image_height, cf, 0, 0, 'imported_image', 'png')
    my_image.close()

def import_jpg_as_jpg(self, my_image):
    my_image_width = my_image.size[0]
    my_image_height = my_image.size[1]
    base64_string = pil_base64(my_image, 'jpeg', 85)
    base64_image_to_canvas(self, base64_string, my_image_width, my_image_height, cf, 0, 0, 'imported_image', 'jpg')
    my_image.close()


def import_file(self):
    import_file_path = builder.get_object('importFileChooserWidget').get_filename()
    # inkex.errormsg(import_file_path)

    filename, file_extension = os.path.splitext(import_file_path)

    # inkex.errormsg(f'{filename} --- {file_extension}')
    if file_extension.lower() == '.txt':
        import_text_file(self, import_file_path)
        return

    else:

        try:
            my_image = Image.open(import_file_path)

            if file_extension.lower() == '.gif':
                # import_gif(self, my_image)
                import_gif_chooser(self, my_image)
                return True
            if file_extension.lower() == '.jpg' or file_extension.lower() == '.jpeg':
                # import_gif(self, my_image)
                import_jpg_as_jpg(self, my_image)
                return True
            else:
                basic_import_file(self, my_image)
                builder.get_object('import_message_label').set_text(f'{import_file_path} **Imported**')
                return True
        except:
            builder.get_object('import_message_label').set_text(f'Unable to open: {import_file_path}')
            return


def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None


def make_temp_svg(self):
    temp_svg_file = tempfile.NamedTemporaryFile(mode='r+', delete='false', suffix='.svg')
    # Write the contents of the updated svg to a tempfile to use with command line
    my_svg_string = self.svg.root.tostring().decode("utf-8")
    temp_svg_file.write(my_svg_string)
    # Essential to refresh file from disk after writing, otherwise get intermittent err
    temp_svg_file.flush()
    return temp_svg_file


def make_master_png(self):
    # Get current selection
    my_selected = self.svg.selected

    # Get user selected export type
    selection_type_radio_group = builder.get_object('export_selection_type_radio').get_group()
    for radio in selection_type_radio_group:
        if radio.get_active():
            selection_type = radio.get_name()

    # current date and time to time stamp
    timestamp = datetime.today().replace(microsecond=0)
    timestamp_suffix = str(timestamp.strftime('%Y-%m-%d-%H-%M-%S'))

    # temp_png_filename
    temp_png_filename = 'temp_png_'

    # Get png dpi setting
    png_dpi = str(builder.get_object('dpi_spin_button').get_value_as_int())

    # Get crop setting
    crop_image_bool = str(builder.get_object('export_crop_cb').get_active())
    # inkex.errormsg(crop_image_bool)
    if crop_image_bool == 'True':
        canvas_to_selection = 'FitCanvasToSelection;'
        is_cropped = 'cropped_'
    else:
        canvas_to_selection = ''
        is_cropped = ''

    # Get invert setting
    invert_selection_bool = str(builder.get_object('export_invert_cb').get_active())

    if invert_selection_bool == 'True':
        invert_selection = ''
        is_inverted = 'inverted_'
    else:
        invert_selection = 'EditInvertInAllLayers;'
        is_inverted = ''

    # inkex.errormsg(canvas_to_selection)

    # my_temp_filename_path = make_temp_svg(my_file_path, my_filename)
    my_temp_svg_file = make_temp_svg(self)
    my_temp_svg_filename_path = my_temp_svg_file.name

    # inkex.errormsg(f'Temp SVG Size: {os.path.getsize(my_temp_svg_filename_path)}')

    # Build a formatted string for command line actions

    # --batch-process ( or --with-gui ) is required if verbs are used in addition to actions
    my_options = '--batch-process'

    my_actions = '--actions='

    export_png_actions = ''

    # Use the global temp folder
    my_export_path = my_temp_folder

    my_png_export_filename_path = my_export_path + '/' + temp_png_filename + timestamp_suffix + '.png'

    if selection_type == 'select_canvas':
        # Actions for default page export
        export_png_actions = my_actions + f' \
        export-filename:{my_png_export_filename_path}; \
        export-dpi:{png_dpi}; \
        export-do'

    if selection_type == 'select_drawing':
        # Actions for default page export
        id_list = []
        for my_object in my_selected:
            id_list.append(my_object.get_id())
        id_list = ','.join(id_list)
        # inkex.errormsg(id_list)
        export_png_actions = my_actions + f' \
        export-filename:{my_png_export_filename_path}; \
        export-area-drawing; \
        export-dpi:{png_dpi}; \
        export-do'

    if selection_type == 'select_selection':
        # Actions for default page export
        id_list = []
        for my_object in my_selected:
            id_list.append(my_object.get_id())
        id_list = ','.join(id_list)
        # inkex.errormsg(id_list)
        export_png_actions = my_actions + f' \
        export-filename:{my_png_export_filename_path}; \
        select-by-id:{id_list}; \
        {invert_selection}; \
        EditDelete; \
        EditSelectAllInAllLayers; \
        {canvas_to_selection} \
        export-dpi:{png_dpi}; \
        export-do'

    export_png_actions = export_png_actions.replace(' ', '')

    # inkex.errormsg(export_png_actions)

    inkex.command.inkscape(my_temp_svg_filename_path, my_options, export_png_actions)

    export_image(my_png_export_filename_path)

    # Close temp file
    my_temp_svg_file.close()
    # Remove the temp folder
    # try:
    #     shutil.rmtree(my_export_path)
    # except:
    #     None


def make_timestamp():
    timestamp = datetime.today().replace(microsecond=0)
    timestamp_suffix = str(timestamp.strftime('%Y-%m-%d-%H-%M-%S'))
    return timestamp_suffix


def read_config_file():
    impex_config = configparser.ConfigParser()
    # Check for existing config file
    if os.path.exists("impex.ini"):
        # print('Found Config File')
        impex_config.read('impex.ini')
    else:
        with open('impex.ini', 'w') as new_config_file:
            new_config_file.close()

    return impex_config


def apply_config(config_object):
    if not config_object.has_section('FORMAT-SETTINGS'):
        config_object['FORMAT-SETTINGS'] = {}
    for my_key in config_object['FORMAT-SETTINGS']:

        my_key_value = config_object['FORMAT-SETTINGS'][my_key]

        my_key_object = builder.get_object(my_key)
        if my_key_object.get_name() == 'GtkCheckButton':

            if my_key_value == 'True':
                my_key_value = True
            else:
                my_key_value = False

            my_key_object.set_active(my_key_value)

        if my_key_object.get_name() == 'GtkSpinButton':
            my_key_object.set_value(int(my_key_value))

        if my_key_object.get_name() == 'GtkComboBox':
            my_key_object.set_active(int(my_key_value))

        if my_key_object.get_name() == 'GtkColorButton':
            my_RGBA = Gdk.RGBA()
            my_RGBA.parse(my_key_value)
            my_key_object.set_rgba(my_RGBA)


def update_config_object(config_object, widget, section):
    widget_type = str(widget.get_name())
    widget_id = str(Gtk.Buildable.get_name(widget))

    # print(f'Widget Type: {widget_type} Widget ID: {widget_id}')

    if not impex_config.has_section(section):
        impex_config[section] = {}

    if widget_type == 'GtkCheckButton':
        widget_state = str(builder.get_object(widget_id).get_active())
        # print(widget_state)
        impex_config[section][widget_id] = widget_state

    if widget_type == 'GtkSpinButton':
        widget_value = str(builder.get_object(widget_id).get_value_as_int())
        # print(widget_value)
        impex_config[section][widget_id] = widget_value

    if widget_type == 'GtkComboBox':
        widget_value = str(builder.get_object(widget_id).get_active())
        # print(widget_value)
        impex_config[section][widget_id] = widget_value

    if widget_type == 'GtkColorButton':
        widget_value = str(builder.get_object(widget_id).get_rgba().to_string())
        # print(widget_value)
        impex_config[section][widget_id] = widget_value


def write_config_file(config_object):
    impex_config = configparser.ConfigParser()
    # Check for existing config file
    if os.path.exists("impex.ini"):
        # print('Found Config File')
        with open('impex.ini', 'w') as configfile:
            config_object.write(configfile)
    else:
        # print('Config File Not Found')
        with open('impex.ini', 'w') as new_config_file:
            new_config_file.close()


# Some export formats require rgb instead of rgba
# Convert to rgb with user colour choice for transparency

def rgba_to_rgb(my_image, gdk_RGBA):
    # print(f'GTK get rgba: {gdk_RGBA.to_color().to_floats()}')

    transparency_color = gdk_RGBA.to_color().to_floats()
    rgb_value = []
    for component in transparency_color:
        rgb_value.append(int(component * 255))
    # print(rgb_value)

    rgba_bg_image = Image.new("RGBA", my_image.size, tuple(rgb_value))
    rgba_bg_image.paste(my_image, (0, 0), my_image)
    my_rgb_image = rgba_bg_image.convert(mode='RGB')
    return my_rgb_image


def export_image(png_path):
    export_path = builder.get_object('exportPathChooserWidget').get_filename()

    # Get user defined export base filename
    export_filename = str(builder.get_object('export_base_filename_entry').get_text())

    export_format_index = builder.get_object('format_combo_box').get_active()
    tree = builder.get_object('export_format_list')
    treeiter = tree.get_iter(export_format_index)
    export_format = tree.get_value(treeiter, 1)

    if export_path != None:
        # print(os.path.isdir(export_path))

        # im = Image.open("/home/name/Pictures/P1000102.JPG")
        # im = Image.open("/home/name/Pictures/Untitled.png")

        im = Image.open(png_path)

        # im = im.resize((224,224), Image.ANTIALIAS)

        timestamp = make_timestamp()

        export_file_path = os.path.join(export_path, export_filename + "_" + timestamp + export_format)

        # im2 = im.save(export_file_path)

        if export_format == '.jpg':
            export_jpeg(im, export_file_path)
        if export_format == '.bmp':
            export_bmp(im, export_file_path)
        if export_format == '.webp':
            export_webp(im, export_file_path)
        if export_format == '.gif':
            export_gif(im, export_file_path)
        if export_format == '.eps':
            export_eps(im, export_file_path)
        if export_format == '.pdf':
            export_pdf(im, export_file_path)
        if export_format == '.tiff':
            export_tiff(im, export_file_path)
        if export_format == '.tga':
            export_tga(im, export_file_path)
        if export_format == '.ico':
            export_ico(im, export_file_path)
        if export_format == '.im':
            export_im(im, export_file_path)
        if export_format == '.png':
            export_png(im, export_file_path)


# -------------------------------------------------------------------------------
# EXPORT FORMAT BLOCKS
# -------------------------------------------------------------------------------

def export_jpeg(my_image, export_file_path):
    transparency_color = builder.get_object('jpeg_transparency_color_picker').get_rgba()
    image_mode = my_image.mode
    if image_mode == 'RGBA':
        my_jpeg_image = rgba_to_rgb(my_image, transparency_color)
    else:
        my_jpeg_image = my_image

    progressive_bool = builder.get_object('jpeg_progressive_cb').get_active()
    optimized_bool = builder.get_object('jpeg_optimize_cb').get_active()
    quality_int = builder.get_object('jpeg_quality_sb').get_value_as_int()
    subsampling_index = builder.get_object('jpeg_subsampling_combo_box').get_active()
    subsampling_tree = builder.get_object('jpeg_subsampling_list')
    treeiter = subsampling_tree.get_iter(subsampling_index)
    subsampling_type = subsampling_tree.get_value(treeiter, 1)
    my_jpeg_image.save(export_file_path, progressive=progressive_bool, quality=quality_int,
                       subsampling=subsampling_type,
                       optimize=optimized_bool)


def export_bmp(my_image, export_file_path):
    my_image.save(export_file_path)


def export_webp(my_image, export_file_path):
    lossless_bool = builder.get_object('webp_lossless_cb').get_active()
    quality_int = builder.get_object('webp_quality_sb').get_value_as_int()
    method_index = builder.get_object('webp_method_combo_box').get_active()
    method_tree = builder.get_object('webp_method_list')
    treeiter = method_tree.get_iter(method_index)
    method_type = method_tree.get_value(treeiter, 1)
    my_image.save(export_file_path, lossless=lossless_bool, quality=quality_int, method=method_type)


def export_gif(my_image, export_file_path):
    gif_image = my_image.convert('P', palette=Image.ADAPTIVE)

    interlaced_bool = builder.get_object('gif_interlace_cb').get_active()
    optimized_bool = builder.get_object('gif_optimise_cb').get_active()
    gif_image.save(export_file_path, interlace=interlaced_bool, optimize=optimized_bool, transparency=255)


def export_eps(my_image, export_file_path):
    transparency_color = builder.get_object('eps_transparency_color_picker').get_rgba()
    image_mode = my_image.mode
    # print(f'Image Mode: {image_mode}')

    if image_mode == 'RGBA':
        my_eps_image = rgba_to_rgb(my_image, transparency_color)
    else:
        my_eps_image = my_image

    my_eps_image.save(export_file_path)


def export_pdf(my_image, export_file_path):
    transparency_color = builder.get_object('pdf_transparency_color_picker').get_rgba()
    quality_int = builder.get_object('pdf_jpeg_quality_sb').get_value_as_int()
    image_mode = my_image.mode
    print(f'Image Mode: {image_mode}')

    if image_mode == 'RGBA':
        my_pdf_image = rgba_to_rgb(my_image, transparency_color)
    else:
        my_pdf_image = my_image
    my_pdf_image.save(export_file_path, quality=quality_int)


def export_tiff(my_image, export_file_path):
    compression_index = builder.get_object('tiff_compression_combo_box').get_active()
    compression_tree = builder.get_object('tiff_compression_list')
    tiff_jpeg_quality_int = builder.get_object('tiff_jpeg_quality_sb').get_value_as_int()
    treeiter = compression_tree.get_iter(compression_index)
    compression_type = compression_tree.get_value(treeiter, 1)
    if compression_type == 'jpeg':
        my_image.save(export_file_path, compression=compression_type, quality=tiff_jpeg_quality_int)
    else:
        my_image.save(export_file_path, compression=compression_type)


def export_tga(my_image, export_file_path):
    my_image.save(export_file_path)


def export_im(my_image, export_file_path):
    my_image.save(export_file_path)


def export_png(my_image, export_file_path):
    my_image.save(export_file_path)


def export_ico(my_image, export_file_path):
    icon_size_list = []

    if builder.get_object('ico_ignore_proportions_cb').get_active():
        my_ico_image = ImageOps.fit(my_image, size=(500, 500))
    else:
        my_ico_image = my_image

    if builder.get_object('ico_16x16_cb').get_active():
        icon_size_list.append((16, 16))
    if builder.get_object('ico_24x24_cb').get_active():
        icon_size_list.append((24, 24))
    if builder.get_object('ico_32x32_cb').get_active():
        icon_size_list.append((32, 32))
    if builder.get_object('ico_48x48_cb').get_active():
        icon_size_list.append((48, 48))
    if builder.get_object('ico_64x64_cb').get_active():
        icon_size_list.append((64, 64))
    if builder.get_object('ico_128x128_cb').get_active():
        icon_size_list.append((128, 128))
    if builder.get_object('ico_256x256_cb').get_active():
        icon_size_list.append((256, 256))

    # print(icon_size_list)

    my_ico_image.save(export_file_path, sizes=icon_size_list)


# -------------------------------------------------------------------------------
# END OF EXPORT FORMAT BLOCKS
# -------------------------------------------------------------------------------


# -------------------------------------------------------------------------------
# HANDLERS
# HANDLERS
# -------------------------------------------------------------------------------

class Handler:
    def on_destroy(self, *args):
        # print('exit')
        write_config_file(impex_config)
        # print(impex_config.sections())
        # Remove the temp folder
        try:
            shutil.rmtree(my_temp_folder)
        except:
            None
        Gtk.main_quit()

    def export_button_click(self, *args):
        make_master_png(inkex_self)

    def import_button_click(self, *args):
        import_file(inkex_self.svg)

    # File browser Handler
    def import_file_folder_change(self, *args):
        selected_folder = builder.get_object('importFileChooserWidget').get_current_folder()
        builder.get_object('importFileChooserWidget').user_selection = selected_folder

    def import_set_recent_folder(self, *args):
        if hasattr(builder.get_object('importFileChooserWidget'), 'user_selection'):
            builder.get_object('importFileChooserWidget').set_current_folder(
                builder.get_object('importFileChooserWidget').user_selection)

    def export_file_folder_change(self, *args):
        selected_folder = builder.get_object('exportPathChooserWidget').get_current_folder()
        builder.get_object('exportPathChooserWidget').user_selection = selected_folder

    def export_set_recent_folder(self, *args):
        if hasattr(builder.get_object('exportPathChooserWidget'), 'user_selection'):
            builder.get_object('exportPathChooserWidget').set_current_folder(
                builder.get_object('exportPathChooserWidget').user_selection)

    # End of File browser Handler

    def get_id(self, widget):
        print((widget.get_name()))
        print(Gtk.Buildable.get_name(widget))

    def update_format_settings(self, widget):
        update_config_object(impex_config, widget, 'FORMAT-SETTINGS')

    def test_handler(self, *args):
        print('test handler')

    def animated_gif_import_window_show(self, *args):
        builder.get_object('animated_gif_import_window').show()
        builder.get_object('main_window').hide()
        # preview_animated_gif()

    def animated_gif_import_window_hide(self, *args):
        builder.get_object('animated_gif_import_window').hide()
        builder.get_object('main_window').show()

    def animation_settings_window_show(self, *args):
        builder.get_object('animation_settings_window').show()
        builder.get_object('animated_gif_import_window').hide()

    def animation_settings_window_hide(self, *args):
        builder.get_object('animated_gif_import_window').show()
        builder.get_object('animation_settings_window').hide()
        return True

    def keep_focus(self, *args):
        builder.get_object('animated_gif_import_window').set_focus()

    def hide_not_delete(self, *args):
        # inkex.errormsg('hello')
        builder.get_object('animated_gif_import_window').hide()
        builder.get_object('main_window').show()
        return True

    def animated_gif_add_to_stack(self, *args):

        preview_animated_gif(self)

    def import_animated_gif(self, *args):
        None

    def import_gif_as_svg_animation(self, *args):
        anim_gif_to_anim_svg(self, True)

    def import_all_gif_frames(self, *args):
        anim_gif_to_anim_svg(self, False)

    def scroll_gif_frame(self, *args):
        frame_number = int(builder.get_object('animated_gif_scale').get_value())
        gif_stack = builder.get_object('animated_gif_stack')
        gif_stack.set_visible_child_name(f'frame_thumbnail_{frame_number}')

    def import_gif_frame(self, *args):
        single_gif_frame_to_canvas(inkex_self)


# -------------------------------------------------------------------------------
# END OF HANDLERS
# END OF HANDLERS
# -------------------------------------------------------------------------------


def run_gtk():
    # Read Config File
    global impex_config
    impex_config = read_config_file()

    # Load stylesheet
    screen = Gdk.Screen.get_default()
    provider = Gtk.CssProvider()
    provider.load_from_path("impex.css")
    Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    global builder
    builder = Gtk.Builder()

    builder.add_from_file('impex.glade')

    global window
    window = builder.get_object("main_window")
    builder.connect_signals(Handler())
    window.connect("destroy", Gtk.main_quit)
    window.show_all()

    # Set initial folder for import and export panes
    home_folder = str(Path.home())
    builder.get_object('importFileChooserWidget').set_current_folder(home_folder)
    builder.get_object('exportPathChooserWidget').set_current_folder(home_folder)

    # Apply Config File

    apply_config(impex_config)

    Gtk.main()


########################################################
#        Inkex effect section                          #
########################################################


class Impex(inkex.EffectExtension):

    def effect(self):
        # Create a global temp folder
        global my_temp_folder
        my_temp_folder = tempfile.mkdtemp()

        global inkex_self
        inkex_self = self

        found_units = self.svg.unit
        global cf
        cf = conversions[found_units]

        run_gtk()


if __name__ == '__main__':
    Impex().run()
